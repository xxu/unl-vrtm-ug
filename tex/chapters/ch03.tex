\newpage
%%% CHAPTER 3
\begin{flushright}
\section{Output Files} \label{sec:outputfiles}
\end{flushright}
\hrule
\vskip0.5in

Once the code runs properly, results will be saved in a single netCDF file, 
the name of which is \code{prefix.unlvrtm.nc}, 
where \code{prefix} is specified in the 
\code{namelist.ini} (Section \ref{sec:diagmenu}). 

Prior to version 1.4.5, however, you should obtain multiple diagnostic files with 
each for one spectrum. Before version 1.4.5, the diagnostic flles are named as
\code{prefix\_LAMDAxxxx.nc}, where \code{xxxx} is spectrum index. 
For single spectrum simulation, \code{xxxx} is \code{0001}. For multi-spectrum 
(say 40), one NetCDF file will be generated for each individual spectrum from 
\code{0001} to \code{0040}. Along with the NetCDF output file, up to two ASCII 
files will be generated with filename of \code{prefix\_LAMDA0001.diaginfo} 
(and \code{prefix.diaginfo} if multi-spectrum). 

Starting from version 1.4.5, model generates a single diagnostic file in 
the netCDF format without any \code{.diaginfo} file. The netCDF file contains
various model variables as controlled by the DIAGNOSTIC menu in the file
\code{namelist.ini} (Section \ref{sec:diagmenu}). This chapter 
presents in detail about the content in the diagnostic file and how to use 
them. In addition, you may also see some additional output text file depending
on the model setting and running status. We will mentioned these files at the
end of this chapter.

\subsection{Output NetCDF file}

The variables saved to the netCDF files are controlled by the Diagnostic Menu
in \code{namelist.ini} for different categories. Here we list
those variables in below Table \ref{tab:ncdims}--\ref{tab:ncdiag08}. The netCDF file also contains a few global 
atrributes (Table \ref{tab:ncglobal}) that could be helpful for quick and 
generic check on the simulation.

\subsection{Other Output files}

UNL-VRTM may generates following files to the run folder:
 \begin{itemize}
  \item \code{VLIDORT\_BRDF\_Check.log} \hspace{1em} This file will
        be generated with messages if BRDF prepared for VLIDORT is problematic.
  \item \code{VLIDORT\_Execution.log}  \hspace{1em} This file will
        be generated with messages if VLIDORT running with problem or warnnings. 
  \item \code{vlidort\_iop.debug} \hspace{1em} This file is generated
        when turning on ``Write VLIDORT inputs" in the DEBUG menu (Section 
        \ref{sec:debugmenu}).
\end{itemize} 

\begin{table}[h]
 \footnotesize
 \centering
 \caption{List of global attributes of the diagnostic netCDF file.}
 \label{tab:ncglobal}
 \begin{tabular}{ p{8em} p{9em} p{23em} }
 \toprule
   Attribute & Value & Description \\
   \midrule
   History   & Created on ...   & File generation date/time \\
   Format    & netCDF3/netCDF4  & netCDF format version \\
   Model     & UNL-VRTM         & Model name \\
   Version   & 1.x.x            & Model version number \\
   VLIODRT   & On/Off           & On/off for radiative transfer simulation \\
   Vector    & On/Off           & If a polarimetric run? \\
   Jacobian  & On/Off           & If a linearized calculation? \\
   Solar Radiation & On/Off     & If including solar source? \\
   Thermal Emission & On/Off    & If including thermal emisison? \\
   Aerosol   & On/Off           & If including aerosol? \\
   Tracegas  & On/Off           & If including tracegas? \\
   Rayleigh  & On/Off           & If including Rayleigh scattering? \\
   Surface   & Lamb/BRDF        & BRDF or Lambertian surface? \\
   Success   & Yes/No           & If simulation is successful? \\
 \bottomrule
 \end{tabular}
\end{table}

%%-----------

\begin{table}[t]
 \footnotesize
 \centering
 \caption{List of dimensions.}
 \label{tab:ncdims}
 \begin{tabular}{ p{9em} p{9em} p{22em} }
 \toprule
  Dimension Name & Value   & Description \\
  \midrule
  Layer & N\_Layers    & Number of atmospheric layers \\
  Level & N\_Layers+1  & Number of atmospheric levels \\ 
  Spectra & NSpectra   & Number of spectrum \\
  Gas     & nGas       & Number of gas species \\
  SZA     & N\_Theta0  & Number of solar zenith angle \\
  VZA     & N\_Theta   & Number of viewing zenith angle \\
  RAZ     & N\_Phi     & Number of relative azmith angle \\
  Mode    & nMode      & Number of aerosol mode \\
  Geo     & n\_Geom    & Number of Sun-viewing geometry \\ 
  Stokes  & n\_Stoks   & Number of Stokes parameters \\
  LOut    & NRecep\_Lev & Number of receptor level \\
  PSDPar  & 3          & Number of PSD parameters \\
  ProfPar & 2          & Number of aerosol profile parameters \\
  Moment  & n\_Moments & Number of Legendre meoments \\
  FMat    & 6          & Number of F-Matrix elements \\
  PAngle  & nPAngle    & Number of phase function angle \\
  BRDFKernel & N\_BRDF & Number of BRDF kernels \\
  LinPar & N\_LPar\_Total & Number of atmospheric Jacobian \\
  LinSPar & N\_LPar\_Surf & Number of surface Jacobian \\
  GMat & nGMMask & Number of Greek matrix elements \\
  LayerRev & N\_Layers    & Reversed atmospheric layers \\
  LevelRev & N\_Layers+1  & Reversed atmospheric levels \\
  Ch6 & 6, & String character of 6 \\
  Ch10 & 10 & String character of 10 \\
  Ch10 & 30 & String character of 30 \\
  Num2 & 2 & Number of 2 \\
  Num2 & 3 & Number of 3 \\
  Num2 & 5 & Number of 5 \\
 \bottomrule
 \end{tabular}
\end{table}

%-----------------

\begin{table}[t]
 \footnotesize
 \centering
 \caption{List of Saved Input Variabls (DIAG01).}
 \label{tab:ncdiag01}
 \begin{tabular}{ p{9em} p{9em} p{22em} }
 \toprule
  Variable  & Units   & Longname  \\
  \midrule
  Lamdas & nm & Spectral wavength \\
  Wavenum & cm$^{-1}$  & Spectral frequency \\
  FWHM & nm or cm$^{-1}$  &  Spectral full-width at half-maximum \\
  IAtm & none & Atmosphere type ID \\
  ZS   & m & Surface altitude (new v1.5.3)\\
  PS   & hPa & Surface pressure (new v1.5.3)\\
  SZA  & degree & Solar zenith angle \\ 
  VZA  & degree & View zenith angle \\
  RAZ  & degree & Relative azimthal angle (see Fig. \ref{fig:geom}) \\
  LOut & \#level & Recptor level \\
  SSC  & none & Single-scatter correction scenario \\
  S0 & none & Solar flux factor \\
  TS & Kelvin & Surface temperature \\
  Gas & none & Tracegas names \\
  GasMW & g mol$^-1$ & Tracegas mole weight \\
  GasSF & none & Tracegas scaling factor \\
  ModeFrc & none & Modal fraction \\
  MR & none & Real-part refractive index \\
  MI & none & Imag-part refractive index \\
  PSDRange & microns & Particle size range \\
  PSDId & none & PSD index \\
  PSDPars & varying & PSD parameters \\
  ProfRange & km & Aerosol vertical extending range \\
  ProfId & none & Aerosol vertical profile type number \\
  ProfPars & varying & Aerosol profile parameters \\
  Ref\_MR & none & Referenced real-part refractive index \\
  Ref\_MI & none & Referenced imag-part refractive index \\
  Ref\_PSDRange & microns & Referenced particle size range \\
  Ref\_PSDId & none & Referenced PSD index \\
  Ref\_PSDPars & varying & Referenced PSD parameters \\
  LinPar & none & Jacobian names \\
 \bottomrule
 \end{tabular}
\end{table}

%----------------

\begin{table}[t]
 \footnotesize
 \centering
 \caption{List of Atmospheric profile variables (DIAG02).}
 \label{tab:ncdiag02}
 \begin{tabular}{ p{9em} p{9em} p{22em} }
 \toprule
  Variable  & Units   & Longname  \\
  \midrule
  Z & km & Level height \\
  P & hPa & Level pressure \\
  T & Kelvin & Level temperature \\
  LayP & hPa & Layer pressure \\
  LayT & Kelvin & Layer temperature \\
  AirDen & molec cm$^{-3}$ & Air number density \\
  GasMR & ppmv &  Gaseous mixing ratio \\
  AerVol & micron$^3$ micron$^{-2}$  & Aerosol volume profile \\
  AerNum & micron$^{-2}$ & Aerosol number profile \\
 \bottomrule
 \end{tabular}
\end{table}

%----------------

\begin{table}[t]
 \footnotesize
 \centering
 \caption{List of Aerosol Scattering Quantities (DIAG03).}
 \label{tab:ncdiag03}
 \begin{tabular}{ p{9em} p{9em} p{22em} }
 \toprule
  Variable  & Units   & Longname  \\
  \midrule
  PAngle & Degree & Equidistant scattering angles \\
  Bulk\_AOP & & Mie/Tmatrix bulk parameters: \\
  & micron$^{-2}$ &  1. Extinction cross section\\
  & micron$^{-2}$ &  2. Scattering cross section \\
  & none &  3. Single scattering albedo \\
  & none &  4. Extinction efficiency \\
  & none &  5. Scattering efficiency \\
  Asymm & none & Asymmetric factor \\
  Dist & & Size distribution parameters: \\
  & & 1. Number Density (normalized to 1)  \\
  & micron$^{-2}$ & 2. Geometric cross section \\
  & micron$^3$ & 3. Volume \\
  & micron & 4. Effective radius \\
  & none & 5. Effective variance \\
  ExpCoeffs & none & Greek matrix expansion coefficients \\
  & & (GK-matrix indexing: 11, 22, 33, 44, -12, 34) \\
  FMatrix & none & Scattering F-matrix \\
  & & (F-matrix indexing: 11, 12, 22, 33, 34, 44) \\
  LPSD\_Bulk &  varying & Bulk\_AOP derivatives wrt PSD \\
  LPSD\_Dist & varying & PSDPars derivatives wrt PSD \\
  LPSD\_FMatrix & varying & FMatrix derivatives wrt PSD \\  
  LRFE\_Bulk & varying & Bulk\_AOP derivatives wrt refractive index \\
  LRFE\_FMatrix & none & FMatrix derivatives wrt refractive index \\
  LFrc\_Bulk & varying & Bulk\_AOP derivatives wrt modal fraction \\
 \bottomrule
 \end{tabular}
\end{table}
 
%----------------

\begin{table}[t]
 \footnotesize
 \centering
 \caption{List of Atmospheric optical profiles (DIAG04).}
 \label{tab:ncdiag04}
 \begin{tabular}{ p{9em} p{9em} p{22em} }
 \toprule
  Variable  & Units   & Longname  \\
  \midrule
  Xsec\_SAO & cm$^2$ molec$^{-1}$ & Gas-absorption cross-section (SAO) \\
  Xsec\_LBL & cm$^2$ molec$^{-1}$ & Gas-absorption cross-section (HITRAN) \\
  Xsec\_CTN & cm$^2$ molec$^{-1}$ & Gas-absorption cross-section (Continuum), new v1.6.0 \\
  Xsec\_Combined & cm$^2$ molec$^{-1}$ & Gas-absorption cross-section combined \\
  tauGas\_SAO & none & Gas-absorption optical depth (SAO) \\
  tauGas\_LBL & none & Gas-absorption optical depth (HITRAN) \\
  tauGas\_CTN & none & Gas-absorption optical depth (Continuum), new v1.6.0 \\
  tauGas\_Combined & none & Gas-absorption optical depth combined \\
  tauGas & none & Bulk gas-absorption optical depth \\
  tauRayleigh & none & Rayleigh-scatting optical depth \\
  Rayleigh\_FMatrix & none & Rayleigh-scatting Greek matrix \\
  tauAER & none & Aerosol optical depth \\
  omegaAER & none & Aerosol single scattering albedo \\
 \bottomrule
 \end{tabular}
\end{table}

%----------------

\begin{table}[t]
 \footnotesize
 \centering
 \caption{List of Surface optical variables (DIAG05).}
 \label{tab:ncdiag05}
 \begin{tabular}{ p{9em} p{9em} p{22em} }
 \toprule
  Variable  & Units   & Longname  \\
  \midrule
  Lamb\_Albedo & none & Lambertian albedo \\
  BRDFKernel & none & BRDF kernel names \\
  BRDFFactor & none & BRDF coefficients \\
  BRDFVal & none & Exact direct BRDF values \\
  SLeaveVal & none & Isotropic surface-leaving radiance \\
 \bottomrule
 \end{tabular}
\end{table}

%----------------

\begin{table}[t]
 \footnotesize
 \centering
 \caption{List of VLIDORT Inputs Variables (DIAG06). }
 \label{tab:ncdiag06}
 \begin{tabular}{ p{9em} p{9em} p{22em} }
 \toprule
  Variable  & Units   & Longname  \\
  \midrule
  GMat & none & Masking index for Greek matrix \\
  tauIOP & none & Bulk atmospheric optical depth \\
  omegaIOP & none & Bulk atmospheric single scattering albedo \\
  GMatIOP & none & Bulk atmospheric scattering Greek matrix \\
  tauLIOP & none & tauIOP linearized inputs \\
  omegaLIOP & none & omegaIOP linearized inputs \\
  GMatLIOP & none & GMatIOP linearized inputs \\
 \bottomrule
 \multicolumn{3}{p{40em}}{Note: For these variables, the dimension for atmospheric layer 
                   starts from top layer and ends at bottom layer.} 
 \end{tabular}
\end{table}


%----------------

\begin{table}[t]
 \footnotesize
 \centering
 \caption{List of Radiative transfer variables (DIAG07).}
 \label{tab:ncdiag07}
 \begin{tabular}{ p{9em} p{9em} p{22em} }
 \toprule
  Variable  & Units   & Longname  \\
  \midrule
  Geo\_Offset & none & VZA offset \\
  Stokes & none & Stokes vector \\
  Mean\_Stokes & none & Stokes mean vector (actinic total flux)\\
  Flux\_Stokes & none & Stokes flux vector (regular total flux)\\
  Mean\_Direct & none & Stokes direct mean vector (actinic direct flux)\\
  Flux\_Direct & none & Stokes direct flux vector (regular direct flux)\\
  Emiss\_Surface & W/m2/sr/um or W/m2/sr/cm$^{-1}$ & Surface thermal emission \\
  GDT\_Emiss\_Surface & W/m2/sr/um/K or W/m2/sr/cm$^{-1}$/K &  Gradient Emiss\_Surface wrt surface T \\
  Emiss\_Atmos & W/m2/sr/um or W/m2/sr/cm$^{-1}$ & Air thermal emission \\
  GDT\_Emiss\_Atmos & W/m2/sr/um/K or W/m2/sr/cm$^{-1}$/K & Gradient Eimss\_Atmos wrt air T \\
 \bottomrule
 \multicolumn{3}{p{40em}}{Note: If thermal calculation turned off, Stokes is unitsless because 
                    it is a normalzied quantity; if thermal calculation turned on, its units
                    should be  (W/m2/sr/um) or (W/m2/sr/cm$^{-1}$) depending on wavelength 
                    or frequency as the model input.}
 \end{tabular}
\end{table}

%----------------

\begin{table}[t]
 \footnotesize
 \centering
 \caption{List of VLIDORT Jacobian variables (DIAG08).}
 \label{tab:ncdiag08}
 \begin{tabular}{ p{9em} p{9em} p{22em} }
 \toprule
  Variable  & Units   & Longname  \\
  \midrule
  Jacob\_Column & same to Stokes & Columnar Jacobians \\
  Jacob\_Prof & same to Stokes & Profile Jacobians \\
  Jacob\_Prof\_Gas & same to Stokes & Gas profile Jacobians \\
  tauAER\_WFS & same to Stokes & AOD profile Jacobians \\
  Jacob\_Surface & same to Stokes & Surface Jacobians \\
 \bottomrule
 \multicolumn{3}{p{40em}}{Note: Jacobian is defined as 
      $x\frac{\partial S}{\partial x}$ for all above variables except 
      Jacob\_Surface. Jacob\_Surface is defined 
      as $\frac{\partial S}{\partial x}$. Here, $S$ is the Stokes vector,
      $x$ is the weighting function denominator. 
      For profile Jacobian variables, the dimension for atmospheric layer
      starts from top layer and ends at bottom layer.}
 \end{tabular}
\end{table}
